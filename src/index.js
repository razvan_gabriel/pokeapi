import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import SearchBar from './components/search_bar';
import PokemonAvatar from './components/pokemon_avatar';
import PokemonDetails from './components/pokemon_details';
import requestPokemon from './methods/requestPokemon';
import PokemonStats from './components/pokemon_stats';

class App extends Component {
    constructor(props){
        super(props);
        this.state = {
            pokemon: {
                name: 'raichu',
                icons: {
                    back: null,
                    front: 'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/26.png'
                },
                stats: {
                    speed: 110,
                    attack: 90,
                    defence: 55,
                    hp: 60
                },
                abilities: [{ability:{name:'lightning-rod'}},{ability:{name:'static'}}]
            }
        };

        this.pokemonSearch('bulbasaur');

        

    }

    pokemonSearch(term) {
        requestPokemon(term.toLowerCase(), (data) => {
            this.setState(
                {
                    pokemon: {
                        name: data.name,
                        icons: {
                            front: data.sprites.front_default
                        },
                        stats: {
                            speed: data.stats[0].base_stat,
                            attack: data.stats[4].base_stat,
                            defence: data.stats[3].base_stat,
                            hp: data.stats[5].base_stat
                        },
                        abilities: data.abilities
                    }

                }
            );
        });
    }

    render(){
        if(!this.state.pokemon.name)
            {
                return(
                    <div className='container'>
                        <div className='row'>
                            <div className='col-md-4 app-logo'><h1>POKEDEX</h1></div>
                            <div className='col-md-8 searchBar'>
                                <SearchBar onSearchTermChange={term => this.pokemonSearch(term)} />
                            </div>
                        </div>
                        <div className='row alert alert-primary' role='alert'>Loading pokemon...</div>
                    </div>
                );
            }
        return(
            <div className='container'>
                <div className='row'>
                    <div className='col-md-4 app-logo'><h1>POKEDEX</h1></div>
                    <div className='col-md-8 searchBar'>
                        <SearchBar onSearchTermChange={term => this.pokemonSearch(term)} />
                    </div>
                </div>
                <div className='row'>
                    <div className='col-md-4 pokemon-avatar'>
                        <PokemonAvatar pokemon={this.state.pokemon}/>
                    </div>
                    <div className='col-md-8 pokemon-details'>
                        <PokemonDetails pokemon={this.state.pokemon}/>
                    </div>
                </div>
                <div className='row'>
                    <div className='col-md-4 pokemon-stats'>
                        <PokemonStats pokemon={this.state.pokemon}/>
                    </div>
                    <div className='col-md-8'>

                    </div>
                </div>
            </div>
        );
    }
}




ReactDOM.render(<App />, document.getElementById('root'));

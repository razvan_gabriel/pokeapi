var axios = require('axios');

var ROOT_URL = 'https://pokeapi.co/api/v2/pokemon/';


module.exports = function (pokemonName, callback) {
    
    var pokemonURL = ROOT_URL + pokemonName;

    axios.get(pokemonURL)
            .then(function (response) {
                if(callback){
                    callback(response.data);
                }
            })
            .catch(function(error) {
                console.error(error);
            })

};
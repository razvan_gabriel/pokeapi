import React from 'react';
import PokemonAbilitiesItem from './pokemon_abilities_item';

const Abilities = (props) => {
    const abilitiesItems = props.abilities.map((abilities) => {
        return <PokemonAbilitiesItem abilities={abilities} />
    });
    
    //console.log(props);
    return(
        <ul className='col-md-4 list-group'>
           {abilitiesItems}
        </ul>
    );
};

export default Abilities;
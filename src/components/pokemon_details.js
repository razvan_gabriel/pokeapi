import React from 'react';
import Abilities from '../components/pokemon_abilities';

const PokemonDetails = (props) => {
    //console.log(props);
    return(
        <div>
            <h3>Abilities:</h3>
            <Abilities abilities={props.pokemon.abilities}/>
        </div>
    );
};

export default PokemonDetails;
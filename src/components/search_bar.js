import React, { Component } from 'react';


class SearchBar extends Component {
    constructor(props) {
        super(props);
        
        this.state = {term: '' };
        
    }
    
    render() {
        return(
            <div>
                <input
                    className='searchForm form-control'
                    value={this.state.term}
                    onChange={event => this.onInputChange(event.target.value)}
                    placeholder='Search for a Pokemon like Bulbasaur'
                />
            </div>
        );
    }
    
    onInputChange(term) {
        this.setState({term});
        this.props.onSearchTermChange(term);
    }
    
}



export default SearchBar;
import React from 'react';

const PokemonAbilitiesItem = (props) => {
    const ability = props.abilities.ability.name;
    return(
        <li className='list-group-item'>
            {ability}
        </li>
    );
};

export default PokemonAbilitiesItem;
import React from 'react';

const PokemonAvatar = (props) => {
    //const pokemonName = props.pokemon[3];
    //const avatarLink = props.pokemon[6];
    //props.pokemon.sprites.front_default
    //{console.log(props.pokemon.sprites.front_default)}
    if(props.pokemon.name == null){
        return <div>Loading...</div>
    }
    
    
    return(
        <div>
            <div className='pokemon-image'>
                <img src={props.pokemon.icons.front} className='img-fluid rounded mx-auto d-block' alt={props.pokemon.name}/>
            </div>
            
            <div className='pokemon-name'>
                <h1>{props.pokemon.name}</h1>
            </div>
        </div>
        
    );
};

export default PokemonAvatar;
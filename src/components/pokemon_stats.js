import React from 'react';

const PokemonStats = (props) => {
    return(
        <div>
            <ul className='list-group'>
                <li className='list-group-item clearfix'> 
                    <span className='float-left font-weight-bold'>SPD:</span>
                    <span className='float-right stats-value'>{props.pokemon.stats.speed}</span>
                </li>
                <li className='list-group-item clearfix'> 
                    <span className='float-left font-weight-bold'>ATTK:</span>
                    <span className='float-right stats-value'>{props.pokemon.stats.attack}</span>
                </li>
                <li className='list-group-item clearfix'> 
                    <span className='float-left font-weight-bold'>DEF:</span>
                    <span className='float-right stats-value'>{props.pokemon.stats.defence}</span>
                </li>
                <li className='list-group-item clearfix'> 
                    <span className='float-left font-weight-bold'>HP:</span>
                    <span className='float-right stats-value'>{props.pokemon.stats.hp}</span>
                </li>
                
            </ul>
        </div>
    );
};

export default PokemonStats;